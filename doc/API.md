# API Documentation

## {+ /api/logbook/\<airfield-code\> +}
**Provide logbook for the given airfield ICAO code and actual airfield date**    
**`Method:`** GET  
**`Protocol:`** HTTP or HTTPS  
**`Parameters:`**
 - `airfield-code:` string with ICAO code upper or lower case  
<!-- end of the list -->
**`Response:`** JSON  
**`Response Code:`** 200 

### {- Response description -}

 ```
 {
  "airfield": {json_airfield_object}
  "date": string,
  "devices": [{json_device_object}, ] or empty array
  "flights": [{json_flight_object}, ] or empty array
}
```
### json_airfield_object
```
{
 "code": string,                            --> ICAO code provided in api request
 "elevation": int or null,                  --> elevation in meters if airfield is recorded in working instance, null otherwise
 "latlng": [float, float] or null,          --> lat & lng of airfield position if airfield is recorded, null otherwise
 "name":string or null,                     --> airfield name if airfield is recorded in working instance, null otherwise
 "tz": string or null                       --> time zone (eg: "Europe/Paris") if airfield is recorded in working instance, null otherwise
}
```
### date

String date format as YYYY-MM-DD.  
Date is calculated with time zone corresponding to airfield location

### json_device_object
```
{
      "address": String,                    --> address of beacon (eg: "DDE42F) 
      "address_origin": String or null,     --> database where beacon is registered (for now, just "OGN") null if not registered
      "aircraft": String or null,           --> aircraft if found in database (eg: "Blanik), null otherwise
      "aircraft_type": int,                 --> int <= 15, description below
      "competition": String or null,        --> competition Name if found in database (eg: "MW), null otherwise
      "identified": Boolean,                --> True if identified option is activated in database, False otherwise
      "registration": String or null,       --> OACI aircraft registration if found in database (eg:"CC-AMW" ), null otherwise 
    }
```
**`aircraft_type:`**  
```
UNKNOWN = 0
GLIDER_OR_MOTOR_GLIDER = 1
TOW_TUG_PLANE = 2
HELICOPTER_ROTORCRAFT = 3
PARACHUTE = 4
DROP_PLANE = 5
HANG_GLIDER = 6
PARA_GLIDER = 7
POWERED_AIRCRAFT = 8
JET_AIRCRAFT = 9
FLYING_SAUCER = 10
BALLOON = 11
AIRSHIP = 12
UNMANNED_AERIAL_VEHICLE = 13
STATIC_OBJECT = 15
```
**`Special note on Privacy concerns`**  
 - If aircraft is not registered in any database, for privacy concerns beacon address is randomly generated.  
Generated string value will be 11 chars long and prefix with \'~\' char
 - If aircraft is registered in OGN database with Identifed flag to False, beacon address is randomy generated.  
Genererated string value will be 11 chars long and prefix with \'_\' char
- If aircraft is registered in OGN database with No tracked option activated ,aircraft will not be part of response api.
### json_flight_object
```
{
      "device": int,              --> index of associated device json_object in devices array
      "duration": int or null,    --> if start & stop are not null, flight duration in seconds. null otherwise
      "max_alt": int or null,     --> maximum altitude in meters reach during flight if start & stop are not null, null otherwise
      "start": String or null,    --> takeoff time formated as HH'h'mm acording to time zone, null if takeoff not detected 
      "start_q": int or null,     --> qfu (eg: 26) use during takeoff, null if takeoff not detected
      "stop": String or null,     --> landing time formated as HH'h'mm acording to time zone, null if landing not detected
      "stop_q": int or null,      --> qfu (eg: 26) use during landing, null if landing not detected
      "tow": int or null,         --> index of associated flight json_object in flights array if aircraft is towed, null otherwise
      "towing": Boolean,          --> true if flight is detected as a towing flight
      "warn": Boolean             --> see below
    },
```
**`warn property`**  
If aircraft is not detected as landed but formally detected on ground on kwnown airfield location, warn is false. True otherwise.  

**`additional stop_code porperty`**
If aircraft is detected landed in another airfield, additional stop_code is added on flight object.  
stop_code value is the OACI airfield code  

**`additional start_code porperty`**  
If aircraft takeoff in another airfield, additional start_code is added on flight object.  
start_code value is the OACI airfield code  
start_code and stop_code properties are mutually exclusive.

## {+ /api/logbook/\<airfield-code\>/\<date\> +}
**Provide logbook for the given airfield ICAO code and given date**    
**`Method:`** GET  
**`Protocol:`** HTTP or HTTPS  
**`Parameters:`**
 - `airfield-code:` string with ICAO code upper or lower case
 - `date:` string date formated as YYYY-MM-DD
 <!-- end of the list -->
**`Response:`** JSON  
**`Response Code:`** 200 

### {- Response description -}
Same as **/api/logbook/\<airfield-code\>** described above.

