# OGN FlightBook

## Project Description

OGN automate flightbook, based on [OGN APRS network](http://wiki.glidernet.org/wiki:ogn-flavoured-aprs).  
Detect takeoff & landing for the most exhaustive airfields locations.  
Project gives the bases for other functionality use in [OGN live](https://live.glidernet.org/).


### Main Goal

Be exhaustive in airfields locations with the smallest possible hardware.  
Ogn Network is free as "free beer" and mostly as "free code". That's why it's seemed interesting to reduce hosting cost and offer a acceptable FOSS solution.
In same time, project need to be evolutive and should offer more functionality with scalability in mind.



### Web Use & Api

A working deployment is currently available: [flightbook.glidernet.org](https://flightbook.glidernet.org)  
[Public API also available](doc/API.md) @ http(s):\/\/flightbook.glidernet.org/api


### Develop

Unlike other monolithic solutions based on a centralized relational database like postgreSQL and his geocoding add-on, the project is based on Redis with a Microservice architecture.  
I really love Redis, it's a wonderfull & powerfull tool which can help to build nice projects.  
Redis is used in logical core parts (as queue manager) but also for geocoding, thank's to the geohash functionnality which is included by default.   
Deployment can be on a single machine or in cluster (native or by containers orchestration).  
The backend part is for now written in Python & Golang and the front js with [vue framework](https://vuejs.org/).  
Python is sometimes not the best choice in term of efficiency and some core parts will maybe re-written in Golang in a next future.
But for now, and thank's to the already [existing python librairies](https://github.com/glidernet/python-ogn-client), it was the quickest way for prototyping and lead to a first functional version.  
More technicals instructions are available in [doc](doc/README.md) part.


### Work in progress
 - Complete airfields list with various missing or incomplete cup files
 - receivers status statistics  

