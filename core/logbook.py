from datetime import datetime, timedelta
from operator import attrgetter

from tabulate import tabulate

from utils.device import get_rdevice


class Flight(object):
    def __init__(self, red, address, start_c, start, start_q, stop_c, stop, stop_q, tz, max_alt=None):
        self.device = get_rdevice(address=address, red=red)
        self.start_c = start_c          # OACI takeoff code
        self.start = start              # POSIX timestamp takeoff
        self.start_q = start_q          # qfu takeoff
        self.stop_c = stop_c            # OACI landing code
        self.stop = stop                # POSIX timestamp landing
        self.stop_q = stop_q            # qfu landing
        self.tow = None                 # associated flight in case of towing (flight object)
        self.max_alt = max_alt          # max altitude reach during flight
        self.is_towing = False          # flag (display purpose)
        self.event_time = None          # use for sorted display
        self.duration = self.stop - self.start if self.stop and self.start is not None else None
        self.tz = tz
        self.warn = False               # warning flag

    @property
    def start_hm(self):
        return datetime.fromtimestamp(self.start, tz=self.tz).strftime('%Hh%M') if self.start is not None else None

    @property
    def stop_hm(self):
        return datetime.fromtimestamp(self.stop, tz=self.tz).strftime('%Hh%M') if self.stop is not None else None

    @property
    def duration_hm(self):
        if self.duration is not None:
            duration_hm = '{}:{}'.format(*(str(timedelta(seconds=self.duration)).split(':')[:2]))
        else:
            duration_hm = None
        return duration_hm


class Logbook(object):
    def __init__(self, date, code, red, db, tz=None):
        """
        Initiate Logbook object for a given date , OACI airfield code & timezone
        date fmt "YYYY-MM-DD", OACI code lower or upper case
        :param date: str
        :param code: str
        :param red: redis client
        :param db: sql db object
        :param tz: any time zone SubClass
        """
        self.tz = tz
        test_date = None
        if date is not None:
            date = date.replace('_', '-').replace('/', '-')
            str_num = date.replace('-', '')
            if len(str_num) == 8 and str_num.isnumeric():
                test_date = date
        if test_date is not None:
            self.date = test_date
        else:
            datetime_ = datetime.now(tz)
            year = datetime_.year
            month = '{:02}'.format(datetime_.month)
            day = '{:02}'.format(datetime_.day)
            self.date = '{}-{}-{}'.format(year, month, day)

        self.code = code.upper()
        self.red = red
        self.db = db
        self.flights = list()

    def create(self):
        events = self.db.get_events(self.date, self.code)
        buffer = dict()     # a buffer use for storing take_off event as buffer[addr]=(take_off_timestamp, qfu, code)
        for addr, code, evt, tsp, max_alt, qfu in events:
            # Landing event
            if evt == 0:
                if addr in buffer:
                    # flight with landing and takeoff
                    flight = Flight(red=self.red, address=addr, start_c=buffer[addr][2], start=buffer[addr][0],
                                    start_q=buffer[addr][1], stop_c=code, stop=tsp, stop_q=qfu,
                                    tz=self.tz, max_alt=max_alt)
                    self.flights.append(flight)
                    del buffer[addr]
                else:
                    # flight without previous takeoff
                    flight = Flight(red=self.red, address=addr, start_c=None, start=None, start_q=None, stop_c=code,
                                    stop=tsp, stop_q=qfu, tz=self.tz)
                    self.flights.append(flight)
            # Takeoff event
            else:
                if addr in buffer:
                    # flight without previous landing
                    flight = Flight(red=self.red, address=addr, start_c=buffer[addr][2], start=buffer[addr][0],
                                    start_q=buffer[addr][1], stop_c=None, stop=None, stop_q=None,  tz=self.tz)
                    self.flights.append(flight)
                buffer[addr] = tsp, qfu, code
        # elements still in buffer: flights without landing
        for addr in buffer:
            flight = Flight(red=self.red, address=addr, start_c=buffer[addr][2], start=buffer[addr][0],
                            start_q=buffer[addr][1], stop_c=None, stop=None, stop_q=None, tz=self.tz)
            self.flights.append(flight)

        # erase not well detect events/flights or external takeoff & landing
        self.flights = [elt for elt in self.flights if self._check_flight(elt)]

        # try to associate towing flights
        buffer_g = list()
        buffer_t = list()
        buffer_ti = list()
        for flight in self.flights:

            if flight.stop_c != self.code:          # delete qfu if external Landing
                flight.stop_q = None

            if flight.start is None or flight.start_c != self.code:
                flight.start_q = None               # delete qfu if external Takeoff
                flight.event_time = flight.stop     # calculate event_time (use for sorted display)
                continue                            # don't use flight without takeoff or with external takeoff
            else:
                flight.event_time = flight.start

            if flight.device.aircraft_type == 1:
                buffer_g.append(flight)
            # try to be as large than possible for bad define tow plane (normally only aircraft_type 2)
            # but eliminate OGN device for tow.
            elif flight.device.address_type != "O":
                buffer_t.append(flight)
                buffer_ti.append(flight.start)

        for flight in buffer_g:
            # protect against min() on empty sequence
            if len(buffer_ti) == 0:
                break
            value = min(buffer_ti, key=lambda x: abs(x - flight.start))     # Closest start value in potential towing
            idx = buffer_ti.index(value)                                    # corresponding closest index
            delta = abs(flight.start - value)
            # TODO maybe add qfu matching for better detection
            if delta < 23:
                flight.tow = buffer_t[idx]
                buffer_t[idx].is_towing = True
                buffer_ti.pop(idx)  # remove elements for working buffers
                buffer_t.pop(idx)

        # sorting flights by event_time
        self.flights = sorted(self.flights, key=attrgetter('event_time'))

    @staticmethod
    def none2str(elt):
        if elt is None:
            return 'UNKNOWN'
        else:
            return elt

    def _check_flight(self, flight):
        duration_check = False if flight.duration is not None and flight.duration < 60 else True
        external_check = False if flight.start_c != self.code and flight.stop_c != self.code else True
        return duration_check and external_check

    def _base_row(self, flight, show_add=False):

        if flight.start_hm and flight.start_c is not None:
            start_hm = flight.start_hm if flight.start_c == self.code else '\n@'.join((flight.start_hm, flight.start_c))
        else:
            start_hm = flight.start_hm

        if flight.stop_hm and flight.stop_c is not None:
            stop_hm = flight.stop_hm if flight.stop_c == self.code else '\n@'.join((flight.stop_hm, flight.stop_c))
        else:
            stop_hm = flight.stop_hm

        row_items = [flight.device.registration, flight.device.aircraft, start_hm, stop_hm, flight.duration_hm]

        if show_add:
            row_items.insert(0, flight.device.address)
        row_items = [self.none2str(elt) for elt in row_items]
        return row_items

    def _ext_raw(self, flight, show_add=False):
        row_items = [flight.device.registration, flight.device.aircraft, flight.stop_hm, flight.duration_hm]
        if show_add:
            row_items.insert(0, flight.device.address)
        row_items = [self.none2str(elt) for elt in row_items]
        return row_items

    def print(self, show_add=False, no_tow=False, glider_only=False):
        print("LogBook {} @ {}:".format(self.date, self.code))
        rows = list()
        flights_ = [elt for elt in self.flights if elt.device.aircraft_type == 1] if glider_only else self.flights
        if no_tow:
            if show_add:
                headers = ('Addr', 'Registr', 'Aircraft', 'Start', 'Stop', 'Duration')
            else:
                headers = ('Registr', 'Aircraft', 'Start', 'Stop', 'Duration')
            for flight in flights_:
                row = self._base_row(flight, show_add=show_add)
                rows.append(row)
            print(tabulate(rows, headers=headers, tablefmt="grid"))

        else:
            if show_add:
                headers = ('Addr', 'Registr', 'Aircraft', 'Start', 'Stop', 'Duration',
                           'Addr', 'Registr', 'Aircraft', 'Stop', 'Duration', 'Aircraft')
            else:
                headers = ('Registr', 'Aircraft', 'Start', 'Stop', 'Duration',
                           'Registr', 'Aircraft', 'Stop', 'Duration', 'Aircraft')
            for flight in flights_:
                if flight.is_towing:
                    continue
                elif flight.tow is not None:
                    row = self._base_row(flight, show_add=show_add) + self._ext_raw(flight.tow, show_add=show_add)
                else:
                    row = self._base_row(flight, show_add=show_add)
                rows.append(row)
            print(tabulate(rows, headers=headers, tablefmt="grid"))
