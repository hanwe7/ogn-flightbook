import json
from datetime import timezone
import logging

from ogn.parser import parse, ParseError

from ogn.client import AprsClient
from setting import BLACK_LIST


def process_beacon(raw_message, red, check):
    try:
        # exclude FANET weather message before parsing (https://github.com/glidernet/ogn-aprs-protocol/issues/8)
        # exclude status messages before parsing
        matches = ('E_', 'W_', ':>')
        if any(m in raw_message for m in matches):
            return

        # TODO check with new python-ogn-client release
        # beacon = parse(raw_message, reference_timestamp=datetime.now(timezone.utc))
        beacon = parse(raw_message)
        if beacon['aprs_type'] != 'position':
            return
        if beacon['beacon_type'] not in ('aprs_aircraft', 'flarm', 'tracker', 'fanet'):
            # logging.debug("beacon_type not evaluate: {}".format(beacon["beacon_type"]))
            return
        if 'address' not in beacon:
            return
        address = beacon['address']
        if address in BLACK_LIST:
            return
        ground_speed = beacon.get('ground_speed', None)
        if ground_speed is None:
            return

        fix = dict()
        # TODO check with new python-ogn-client release
        # fix['tsp'] = int(datetime.timestamp(beacon['timestamp']))
        fix['tsp'] = int(beacon['timestamp'].replace(tzinfo=timezone.utc).timestamp())

        # multi-receivers case (height receiver density)=> we have to sort aircraft messages by timestamp
        # messages could be unordered when process, furthermore we can have 2 same records
        old_tsp = check.get(address, 0)
        if fix['tsp'] <= old_tsp:
            return
        check[address] = fix['tsp']

        fix['alt'] = round(beacon['altitude']) if beacon['altitude'] is not None else None
        fix['lat'] = beacon['latitude']
        # lat is limited for areas very near to the poles
        # read: https://redis.io/commands/geoadd
        # exclude beacon message in this case
        if abs(beacon['latitude']) > 85.05:
            return
        fix['lng'] = beacon['longitude']
        clr = beacon.get('climb_rate', None)
        fix['clr'] = round(clr, 2) if clr is not None else None
        fix['gsp'] = round(ground_speed)
        fix['track'] = beacon['track']
        fix['steal'] = beacon.get('stealth', False)

        # use redis pipeline for speed-up
        p = red.pipeline(transaction=False)     # python pipeline implementation use transactional by default
        p.rpush('BA:{}'.format(address), json.dumps(fix))
        p.geoadd('GEO:A', beacon['longitude'], beacon['latitude'], address)
        p.sadd('SQBA', 'BA:{}'.format(address))
        p.execute()

    except ParseError as e:
        logging.debug(repr(e))
    except NotImplementedError as e:
        logging.debug("{}\nraw message: {}".format(repr(e), raw_message))


def run(red):
    check = dict()
    client = AprsClient(aprs_user='NOCALL')
    client.connect()
    while True:
        try:
            client.run(callback=process_beacon, autoreconnect=True, red=red, check=check)
        except KeyboardInterrupt:
            print('\nStop ogn gateway')
            client.disconnect()
            break
        except BaseException as e:
            logging.warning("Error not catch in run method: {}".format(repr(e)))
