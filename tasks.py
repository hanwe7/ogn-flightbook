from time import sleep
import logging

import click
import redis

from utils.db import Db
import setting


@click.command()
@click.argument('cmd')
@click.option('-d', '--debug', help='debug option (no delete queue elements)', is_flag=True)
@click.option('-v', '--verbose', help='logging verbose', is_flag=True)
def main(cmd, debug, verbose):
    """
    CMD=aprs or CMD=feed or CMD=cons
    respectively for reveive aprs , feeding or consum queue
    """
    if verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    pool = redis.ConnectionPool(host=setting.REDIS_HOST, port=setting.REDIS_PORT, db=setting.REDIS_DB,
                                decode_responses=True, encoding="utf-8")
    red = redis.Redis(connection_pool=pool)
    try:
        red.ping()
    except BaseException as e:
        print("error while testing redis connection")
        print("check redis server is up and running")
        logging.debug(str(e))

    db = Db(path=setting.SQLITE_PATH)
    if cmd == "aprs":
        from core.receiver import run
        run(red=red)

    elif cmd == "feed":
        from core.queue_ import feed_queue
        if debug:
            red.delete('QBA')
        while True:
            feed_queue(red, delete=not debug)
            if debug:
                break
            sleep(1)

    elif cmd == "cons":
        from core.queue_ import consum_queue
        while True:
            try:
                consum_queue(red=red, db=db, delete=not debug)
            except KeyboardInterrupt:
                print("Stop consumer process")
                break
            except BaseException as e:
                logging.warning(repr(e))
    else:
        ctx = click.get_current_context()
        print(ctx.get_help())


if __name__ == "__main__":
    main()
