import ctypes
from json import loads

from pytz import timezone as pytz_timezone, utc as pytz_utc

from core.logbook import Logbook
from utils.device import mapping_addresses, Device


class WLogbook(Logbook):
    """
    a SubClass of LogBook with method for json export and airfield class member
    json export take care of privacy
    """
    def __init__(self, date, code, red, db):
        airfield = {"code": code, "name": None, "elevation": None, "tz": None, "latlng": None}
        match_airfield = db.match_airfield(code)
        if match_airfield is not None:
            airfield["name"] = match_airfield[0]
            airfield["elevation"] = match_airfield[1]
            airfield["tz"] = match_airfield[2]
            lnglats = red.geopos("GEO:L", code)
            if len(lnglats) == 1:
                airfield["latlng"] = [round(x, 5) for x in lnglats[0][::-1]]
        tz = pytz_timezone(airfield['tz']) if airfield['tz'] is not None else pytz_utc
        Logbook.__init__(self, date, code, red, db, tz)
        self.airfield = airfield

    def to_json(self):
        """
        Json string repr for flightbook. Circular refs to flight or nested devices objects
        are identified as index of respective flights or devices json array.
        ie: {devices:[{first_json_device, second_json_device], ...]
             flights: [{device: 1, ...}, {device: 0, ..}, ...]}

        :return: string
        """

        addresses = [flight.device.address for flight in self.flights]
        warn = dict()
        uniq_addresses = list(set(addresses))
        if len(uniq_addresses) != 0:
            idx = 0
            for elt in self.red.hmget(name='STS', keys=uniq_addresses):
                if elt is None:
                    warn[uniq_addresses[idx]] = False
                else:
                    status = loads(elt)
                    # if detected landed or formally on ground on a known airfield, warn false
                    if status['le'] == 0 or status['le'] == 2:
                        warn[uniq_addresses[idx]] = False
                    # if detected takeoff or on ground on a unknown place, warn true
                    elif status['le'] == 1 or status['le'] == 3:
                        warn[uniq_addresses[idx]] = True
                    else:
                        warn[uniq_addresses[idx]] = False
                idx += 1

        # privacy concerns, check against mapping address
        # find some privacy test cases with following sql requests
        # select * from Events where address IN (select address from Devices where tracked=0) and code is not NULL
        map_addresses = mapping_addresses(addresses=addresses, red=self.red)
        idx = 0
        for address in addresses:
            self.flights[idx].warn = warn[address]
            # case tracked = False => delete flight
            if map_addresses[idx] == '-1':
                self.flights[idx] = None
            # case not present in ddb or identified = False
            elif address != map_addresses[idx]:
                # create a generic device with obfuscated address
                self.flights[idx].device = Device(map_addresses[idx])
            idx += 1

        json = dict()
        json['airfield'] = self.airfield
        json['code'] = self.code
        json['date'] = self.date
        devices_id = list()
        flights_id = list()
        jflights = list()

        for flight in self.flights:
            # ignore None flights (marked as deleted by mapping)
            if flight is None:
                continue
            flights_id.append(id(flight))
            jflight = dict()
            jflight['device'] = id(flight.device)
            devices_id.append(id(flight.device))
            if flight.start_c != self.code and flight.start_c is not None:
                jflight['start_code'] = flight.start_c
            jflight['start'] = flight.start_hm
            jflight['start_q'] = flight.start_q
            if flight.stop_c != self.code and flight.stop_c is not None:
                jflight['stop_code'] = flight.stop_c
            jflight['stop'] = flight.stop_hm
            jflight['stop_q'] = flight.stop_q
            jflight['tow'] = id(flight.tow) if flight.tow is not None else None
            jflight['towing'] = flight.is_towing
            jflight['duration'] = flight.duration
            jflight['max_alt'] = flight.max_alt
            jflight['max_hight'] = abs(flight.max_alt - self.airfield.get("elevation", 0)) if \
                flight.max_alt is not None else None
            jflight['warn'] = flight.warn
            jflights.append(jflight)
        json['flights'] = jflights
        # create a list (ordered) of unique device_id
        devices_id = list(set(devices_id))
        # create a list by deferencing device object
        json["devices"] = [ctypes.cast(elt, ctypes.py_object).value.__dict__ for elt in devices_id]
        # replace object ref by index position in respective list
        for jflight in json['flights']:
            jflight['device'] = devices_id.index(jflight['device'])
            if jflight['tow'] is not None:
                jflight['tow'] = flights_id.index(jflight['tow'])
        return json
