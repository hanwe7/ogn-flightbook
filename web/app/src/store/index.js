import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
let id = 0
export default new Vuex.Store({
  state: {
    date: null,
    airfield: {},
    socket: {isConnected: false, reconnectError: false, flightevts:[]}
  },
  mutations: {
    save (state, obj) {
      state.airfield = obj.airfield
      state.date = obj.date
    },
    SOCKET_ONOPEN (state, event)  {
      Vue.prototype.$socket = event.currentTarget
      state.socket.isConnected = true
    },
    SOCKET_ONCLOSE (state, event)  {
      console.log(event)
      state.socket.isConnected = false
    },
    SOCKET_ONERROR (state, event)  {
      console.error(state, event)
    },
    // default handler called for all methods
    SOCKET_ONMESSAGE (state, message)  {
      // dot not process when browser window is not activated
      // else got a bloody mess in DOM when wake up + client work for nothing
      if (document.hidden) return
      if (message.type ===1){
        const elt = message.codevt[0]
        id += 1
        elt.id = id
        state.socket.flightevts.push(elt)
        if (state.socket.flightevts.length >= 5) state.socket.flightevts.shift()
        }
      else if (message.type ===0){
      if (message.codevt !==null) {
        id = 0
        state.socket.flightevts = []
        for (const elt of message.codevt){
            id += 1
            elt.id = id
            state.socket.flightevts.push(elt)
            }
      }
    }},
    // mutations for reconnect methods
    SOCKET_RECONNECT(state, count) {
      console.info(state, count)
    },
    SOCKET_RECONNECT_ERROR(state) {
      state.socket.reconnectError = true;
    },
  },
  actions: {
  },
  modules: {
  }
})
