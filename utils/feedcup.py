from os.path import dirname, abspath, join, basename
import glob

from aerofiles.seeyou import Reader
from pytz import country_timezones, all_timezones
from timezonefinder import TimezoneFinder


class MyReader(Reader):
    def __init__(self, fp=None):
        """
        Reader subclass with modification of decode_runaway_direction &  decode_frequency
        as cup files are note always clean and we don't need theses values in our code
        :param fp: file object
        """
        Reader.__init__(self, fp)

    def decode_runway_direction(self, runway_direction):
        return None

    def decode_frequency(self, frequency):
        return None


def feed_file(red, db, file):
    with open(file=file, mode='r') as file_cup:
        reader = MyReader().read(fp=file_cup)
        nb = 0
        codes = dict()
        for waypoint in reader['waypoints']:
            if waypoint['style'] not in [2, 4, 5]:
                continue
            if waypoint.get('code', None) is None:
                continue
            name = waypoint.get('name', None)

            elevation = waypoint.get('elevation', None)
            if elevation is None:
                continue
            nb += 1
            if elevation['unit'] == 'ft':
                elevation = round(elevation['value']*0.3048)
            else:
                elevation = round(elevation['value'])

            country = waypoint.get('country', None)
            if country is None:
                tz = None
            else:
                timezones = country_timezones[country]
                # some contries have just one time zone, fast to obtain with pytz
                # Germany Berlin & Busingen => same offset (https://en.wikipedia.org/wiki/Time_in_Germany)
                if len(timezones) == 1 or country == 'DE':
                    tz = timezones[0]
                else:
                    # some countries can have multiple time_zone
                    # we need to retrieve each time zone by position => time consuming.
                    # TimezoneFinder (with optional numba) is the fastest python implementation found
                    tf = TimezoneFinder()
                    tz = tf.timezone_at(lng=waypoint['longitude'], lat=waypoint['latitude'])

            tz_index = all_timezones.index(tz)
            codes[waypoint['code']] = name, elevation, tz, tz_index
            red.geoadd("GEO:L",  waypoint['longitude'], waypoint['latitude'], waypoint['code'].upper())

        db.replace_airfields(codes)
        print("{} nb points saved".format(nb))


def feed_dir(db, red, cup_dir=None, filename=None):
    """

    :param db: database object
    :param red: redis client
    :param cup_dir:
    :param filename:
    :return:
    """
    if cup_dir is None:
        current_dir = dirname(abspath(__file__))
        cup_dir = join(current_dir, '../cup')

    if filename is None:
        cup_files = glob.glob('{}/*.cup'.format(cup_dir))
    else:
        cup_files = (join(cup_dir, filename),)

    for cup_file in cup_files:
        print("adding {}".format(basename(cup_file)))
        feed_file(red, db, cup_file)
        print("-" * 30)
    red.close()
    db.close()
